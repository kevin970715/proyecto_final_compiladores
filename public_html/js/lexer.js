/* lexical grammar */
%lex
%%


\<!.*\>                     /* saltar los comentarios*/  
\s+                         /* skip whitespace */
\".*\"                      return 'constante'
\'.*\'                      return 'constante'
"decla"                     return 'decla'
"si"                        return 'si'
"sino"                      return 'sino'
"bandera"                   return 'bandera'
"caso"                      return 'caso'
"cortar"                    return 'cortar'
"pordefecto"                return 'pordefecto'
"pantallazo"                return 'pantallazo'
"c_w"                       return 'c_w'
"c_f"                       return 'c_f'
"hacer"                     return 'hacer'
"nuevo"                     return 'nuevo'
"arreglo"                   return 'arreglo'
"||"                        return 'OP_LOGICO'
"or"                        return 'OP_LOGICO'
"and"                       return 'OP_LOGICO'
"&&"                        return 'OP_LOGICO'
\-*[0-9]+("."[0-9]+)?\b     return 'NUMERO'
"<-"                        return '<-'
"+"                         return '+'
"-"                         return '-'
"*"                         return '*'
"/"                         return '/'
"("                         return '('
")"                         return ')'
"["                         return '['
"]"                         return ']'
"{"                         return '{'
"}"                         return '}'
":"                         return ':'
","                         return ','
"=="                        return 'CONDICION_OP'
">"                         return 'CONDICION_OP'
"<"                         return 'CONDICION_OP'
">="                        return 'CONDICION_OP'
"<="                        return 'CONDICION_OP'
"!="                        return 'CONDICION_OP'
"<>"                        return 'CONDICION_OP'
[A-Za-z]+[0-9]*             return 'IDENTIFICADOR'
<<EOF>>                     return 'EOF'
.                           return 'INVALID'

/lex


