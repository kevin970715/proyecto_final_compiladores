
%start expressions

%% /* language grammar */

expressions
    : ORDEN EOF
        {console.log("success"); return $1;}
    ;

ORDEN
    : IMPRIMIR
    | DECLARACION
    | ASIGNACION
    | ASIGNACIONV
    | ACUMULADORMAS
    | ACUMULADOR
    | CONDICION
    | BANDERA
    | CICLOW
    | DW
    | CICLOF
    ;


    
    IMPRIMIR
    : 'pantallazo''('C_IMPRIMIR')'','
    | 'pantallazo''('C_IMPRIMIR')'',' ORDEN
    ;

    C_IMPRIMIR
    : CONSTANTE
    | OPERADOR
    | CONSTANTE '+' C_IMPRIMIR
    | OPERADOR OPERANDO C_IMPRIMIR
    | OPERACIONV OPERANDO C_IMPRIMIR
    ;

    DECLARACION
        : 'decla' IDENTIFICADOR',' 
        | 'decla' IDENTIFICADOR',' ORDEN
        | 'decla' ASIGNACION 
        | 'decla' ASIGNACIONV
        ;

    ASIGNACION
        : IDENTIFICADOR'<-'OPERADOR','
        | IDENTIFICADOR'<-'OPERADOR',' ORDEN
        | IDENTIFICADOR'<-''constante'','
        | IDENTIFICADOR'<-''constante'',' ORDEN
        ;
    
    ASIGNACIONV
        : IDENTIFICADOR'<-''{'CONTENTV'}'','
        | IDENTIFICADOR'<-''{'CONTENTV'}'',' ORDEN
        | IDENTIFICADOR'<-''{''}'','
        | IDENTIFICADOR'<-''{''}'',' ORDEN
        | IDENTIFICADOR '<-''nuevo' 'arreglo''('CONTENTV')'','
        | IDENTIFICADOR '<-''nuevo' 'arreglo''('CONTENTV')'',' ORDEN
        | IDENTIFICADOR'{'OPERADOR'}' '<-''nuevo' 'arreglo''('CONTENTV')'','
        | IDENTIFICADOR'{'OPERADOR'}' '<-''nuevo' 'arreglo''('CONTENTV')'',' ORDEN
        | IDENTIFICADOR'{'OPERADOR'}' '<-' OPERACIONVEC
        ;

        CONTENTV
            : CONSTANTE
            | OPERADOR
            | CONSTANTE CONTENTV
            | OPERADOR CONTENTV
            | OPERADOR OPERANDO CONTENTV
            | OPERACIONV
            | OPERACIONV OPERANDO CONTENTV
            ;

        OPERACIONVEC
            : OPERACIONV
            | IDENTIFICADOR'{'OPERADOR'}'',' ORDEN
            ;

        OPERACIONV
            : IDENTIFICADOR'{'OPERADOR'}'','
            | IDENTIFICADOR'{'OPERADOR'}'OPERANDO OPERACIONV
            ;

            CONSTANTE
                : 'constante'','
                | 'constante'
                ;

    ACUMULADORMAS
        : IDENTIFICADOR'+''+'',' ORDEN
        | IDENTIFICADOR'-''-'',' ORDEN
        | IDENTIFICADOR'<-'IDENTIFICADOR OPERANDO OPERADOR',' ORDEN
        ;

    ACUMULADOR
        : IDENTIFICADOR'+''+'','
        | IDENTIFICADOR'-''-'','
        | IDENTIFICADOR'<-'IDENTIFICADOR OPERANDO OPERADOR','
        ;

        OPERANDO
            :'+'
            |'-'
            |'*'
            |'/'
            ;

    CONDICION
        :'si''('COMPARACIONES')''['ORDEN']' 
        |'si''('COMPARACIONES')''['ORDEN']' SIG_CONDICION
        |'si''('COMPARACIONES')''['ORDEN']' ORDEN
        ;

        SIG_CONDICION
            : 'sino' '[' ORDEN ']' 
            | 'sino' '[' ORDEN ']'SIG_CONDICION 
            | 'sino' '[' ORDEN ']'ORDEN
            | 'sino' 'si' '(' COMPARACIONES ')' '[' ORDEN ']'
            | 'sino' 'si' '(' COMPARACIONES ')' '[' ORDEN ']' SIG_CONDICION
            | 'sino' 'si' '(' COMPARACIONES ')' '[' ORDEN ']' ORDEN
            ;

    BANDERA
        : 'bandera''('IDENTIFICADOR')''['CASOS DEFAULT']'
        | 'bandera''('IDENTIFICADOR')''['CASOS DEFAULT']' ORDEN
        ;

        CASOS
            : 'caso' OPERADOR ':' ORDEN 'cortar'','
            | 'caso' OPERADOR ':' ORDEN 'cortar'','CASOS
            ;

        DEFAULT 
            : 'pordefecto'':' ORDEN
            ;

    CICLOW
        : 'c_w''('COMPARACIONES')''['ORDEN']'
        | 'c_w''('COMPARACIONES')''['ORDEN']'ORDEN
        ;
    DW
        : 'hacer''['ORDEN']'CICLOW','
        ;

    CICLOF
        : 'c_f''(''decla' IDENTIFICADOR'<-'OPERADOR',' COMPARACION',' ACUMULADOR')''['']'
        | 'c_f''(''decla' IDENTIFICADOR'<-'OPERADOR',' COMPARACION',' ACUMULADOR')''['']' ORDEN
        | 'c_f''(''decla' IDENTIFICADOR'<-'OPERADOR',' COMPARACION',' ACUMULADOR')''['ORDEN']'
        | 'c_f''(''decla' IDENTIFICADOR'<-'OPERADOR',' COMPARACION',' ACUMULADOR')''['ORDEN']' ORDEN
        | 'c_f''('IDENTIFICADOR'<-'OPERADOR',' COMPARACION',' ACUMULADOR')''['']'
        | 'c_f''('IDENTIFICADOR'<-'OPERADOR',' COMPARACION',' ACUMULADOR')''['']' ORDEN
        | 'c_f''('IDENTIFICADOR'<-'OPERADOR',' COMPARACION',' ACUMULADOR')''['ORDEN']'
        | 'c_f''('IDENTIFICADOR'<-'OPERADOR',' COMPARACION',' ACUMULADOR')''['ORDEN']' ORDEN
        ;

        COMPARACIONES
            : OPERADOR CONDICION_OP OPERADOR OP_LOGICO COMPARACIONES
            | COMPARACION
            ;

        COMPARACION
            :  OPERADOR CONDICION_OP OPERADOR 
            ;


                OPERADOR
                    : IDENTIFICADOR
                    | NUMERO
                    ;