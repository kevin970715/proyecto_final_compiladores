/* parser generated by jison 0.4.13 */
/*
  Returns a Parser object of the following structure:

  Parser: {
    yy: {}
  }

  Parser.prototype: {
    yy: {},
    trace: function(),
    symbols_: {associative list: name ==> number},
    terminals_: {associative list: number ==> name},
    productions_: [...],
    performAction: function anonymous(yytext, yyleng, yylineno, yy, yystate, $$, _$),
    table: [...],
    defaultActions: {...},
    parseError: function(str, hash),
    parse: function(input),

    lexer: {
        EOF: 1,
        parseError: function(str, hash),
        setInput: function(input),
        input: function(),
        unput: function(str),
        more: function(),
        less: function(n),
        pastInput: function(),
        upcomingInput: function(),
        showPosition: function(),
        test_match: function(regex_match_array, rule_index),
        next: function(),
        lex: function(),
        begin: function(condition),
        popState: function(),
        _currentRules: function(),
        topState: function(),
        pushState: function(condition),

        options: {
            ranges: boolean           (optional: true ==> token location info will include a .range[] member)
            flex: boolean             (optional: true ==> flex-like lexing behaviour where the rules are tested exhaustively to find the longest match)
            backtrack_lexer: boolean  (optional: true ==> lexer regexes are tested in order and for each matching regex the action code is invoked; the lexer terminates the scan when a token is returned by the action code)
        },

        performAction: function(yy, yy_, $avoiding_name_collisions, YY_START),
        rules: [...],
        conditions: {associative list: name ==> set},
    }
  }


  token location info (@$, _$, etc.): {
    first_line: n,
    last_line: n,
    first_column: n,
    last_column: n,
    range: [start_number, end_number]       (where the numbers are indexes into the input string, regular zero-based)
  }


  the parseError function receives a 'hash' object with these members for lexer and parser errors: {
    text:        (matched text)
    token:       (the produced terminal token, if any)
    line:        (yylineno)
  }
  while parser (grammar) errors will also provide these members, i.e. parser errors deliver a superset of attributes: {
    loc:         (yylloc)
    expected:    (string describing the set of expected tokens)
    recoverable: (boolean: TRUE when the parser has a error recovery rule available for this particular error)
  }
*/
var parser = (function(){
var parser = {trace: function trace(){},
yy: {},
symbols_: {"error":2,"expressions":3,"ORDEN":4,"EOF":5,"IMPRIMIR":6,"DECLARACION":7,"ASIGNACION":8,"ASIGNACIONV":9,"ACUMULADORMAS":10,"ACUMULADOR":11,"CONDICION":12,"BANDERA":13,"CICLOW":14,"DW":15,"CICLOF":16,"pantallazo":17,"(":18,"C_IMPRIMIR":19,")":20,",":21,"CONSTANTE":22,"OPERADOR":23,"+":24,"OPERANDO":25,"OPERACIONV":26,"decla":27,"IDENTIFICADOR":28,"<-":29,"constante":30,"{":31,"CONTENTV":32,"}":33,"nuevo":34,"arreglo":35,"OPERACIONVEC":36,"-":37,"*":38,"/":39,"si":40,"COMPARACIONES":41,"[":42,"]":43,"SIG_CONDICION":44,"sino":45,"bandera":46,"CASOS":47,"DEFAULT":48,"caso":49,":":50,"cortar":51,"pordefecto":52,"c_w":53,"hacer":54,"c_f":55,"COMPARACION":56,"CONDICION_OP":57,"OP_LOGICO":58,"NUMERO":59,"$accept":0,"$end":1},
terminals_: {2:"error",5:"EOF",17:"pantallazo",18:"(",20:")",21:",",24:"+",27:"decla",28:"IDENTIFICADOR",29:"<-",30:"constante",31:"{",33:"}",34:"nuevo",35:"arreglo",37:"-",38:"*",39:"/",40:"si",42:"[",43:"]",45:"sino",46:"bandera",49:"caso",50:":",51:"cortar",52:"pordefecto",53:"c_w",54:"hacer",55:"c_f",57:"CONDICION_OP",58:"OP_LOGICO",59:"NUMERO"},
productions_: [0,[3,2],[4,1],[4,1],[4,1],[4,1],[4,1],[4,1],[4,1],[4,1],[4,1],[4,1],[4,1],[6,5],[6,6],[19,1],[19,1],[19,3],[19,3],[19,3],[7,3],[7,4],[7,2],[7,2],[8,4],[8,5],[8,4],[8,5],[9,6],[9,7],[9,5],[9,6],[9,8],[9,9],[9,11],[9,12],[9,6],[32,1],[32,1],[32,2],[32,2],[32,3],[32,1],[32,3],[36,1],[36,6],[26,5],[26,6],[22,2],[22,1],[10,5],[10,5],[10,7],[11,4],[11,4],[11,6],[25,1],[25,1],[25,1],[25,1],[12,7],[12,8],[12,8],[44,4],[44,5],[44,5],[44,8],[44,9],[44,9],[13,8],[13,9],[47,6],[47,7],[48,3],[14,7],[14,8],[15,6],[16,13],[16,14],[16,14],[16,15],[16,12],[16,13],[16,13],[16,14],[41,5],[41,1],[56,3],[23,1],[23,1]],
performAction: function anonymous(yytext, yyleng, yylineno, yy, yystate /* action[1] */, $$ /* vstack */, _$ /* lstack */
/*``*/) {
/* this == yyval */

var $0 = $$.length - 1;
switch (yystate) {
case 1: return "ESCRITO CORRECTAMENTE!!!";
break;
}
},
table: [{3:1,4:2,6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],27:[1,15],28:[1,16],40:[1,17],46:[1,18],53:[1,19],54:[1,20],55:[1,21]},{1:[3]},{5:[1,22]},{5:[2,2],21:[2,2],43:[2,2],51:[2,2]},{5:[2,3],21:[2,3],43:[2,3],51:[2,3]},{5:[2,4],21:[2,4],43:[2,4],51:[2,4]},{5:[2,5],21:[2,5],43:[2,5],51:[2,5]},{5:[2,6],21:[2,6],43:[2,6],51:[2,6]},{5:[2,7],21:[2,7],43:[2,7],51:[2,7]},{5:[2,8],21:[2,8],43:[2,8],51:[2,8]},{5:[2,9],21:[2,9],43:[2,9],51:[2,9]},{5:[2,10],21:[2,10],43:[2,10],51:[2,10]},{5:[2,11],21:[2,11],43:[2,11],51:[2,11]},{5:[2,12],21:[2,12],43:[2,12],51:[2,12]},{18:[1,23]},{8:25,9:26,28:[1,24]},{24:[1,29],29:[1,27],31:[1,28],37:[1,30]},{18:[1,31]},{18:[1,32]},{18:[1,33]},{42:[1,34]},{18:[1,35]},{1:[2,1]},{19:36,22:37,23:38,26:39,28:[1,41],30:[1,40],59:[1,42]},{21:[1,43],29:[1,44],31:[1,28]},{5:[2,22],21:[2,22],43:[2,22],51:[2,22]},{5:[2,23],21:[2,23],43:[2,23],51:[2,23]},{23:45,28:[1,49],30:[1,46],31:[1,47],34:[1,48],59:[1,42]},{23:50,28:[1,51],59:[1,42]},{24:[1,52]},{37:[1,53]},{23:55,28:[1,51],41:54,56:56,59:[1,42]},{28:[1,57]},{23:55,28:[1,51],41:58,56:56,59:[1,42]},{4:59,6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],27:[1,15],28:[1,16],40:[1,17],46:[1,18],53:[1,19],54:[1,20],55:[1,21]},{27:[1,60],28:[1,61]},{20:[1,62]},{20:[2,15],24:[1,63]},{20:[2,16],24:[1,65],25:64,37:[1,66],38:[1,67],39:[1,68]},{24:[1,65],25:69,37:[1,66],38:[1,67],39:[1,68]},{20:[2,49],21:[1,70],24:[2,49],28:[2,49],30:[2,49],33:[2,49],59:[2,49]},{20:[2,88],24:[2,88],28:[2,88],30:[2,88],31:[1,71],33:[2,88],37:[2,88],38:[2,88],39:[2,88],59:[2,88]},{20:[2,89],21:[2,89],24:[2,89],28:[2,89],30:[2,89],33:[2,89],37:[2,89],38:[2,89],39:[2,89],50:[2,89],57:[2,89],58:[2,89],59:[2,89]},{4:72,5:[2,20],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,20],27:[1,15],28:[1,16],40:[1,17],43:[2,20],46:[1,18],51:[2,20],53:[1,19],54:[1,20],55:[1,21]},{23:45,28:[1,51],30:[1,46],31:[1,47],34:[1,48],59:[1,42]},{21:[1,73]},{21:[1,74]},{22:77,23:78,26:79,28:[1,41],30:[1,40],32:75,33:[1,76],59:[1,42]},{35:[1,80]},{21:[2,88],24:[1,65],25:81,37:[1,66],38:[1,67],39:[1,68]},{33:[1,82]},{20:[2,88],21:[2,88],33:[2,88],50:[2,88],57:[2,88],58:[2,88]},{21:[1,83]},{21:[1,84]},{20:[1,85]},{57:[1,86]},{20:[2,86]},{20:[1,87]},{20:[1,88]},{43:[1,89]},{28:[1,90]},{29:[1,91]},{21:[1,92]},{19:93,22:37,23:38,26:39,28:[1,41],30:[1,40],59:[1,42]},{19:94,22:37,23:38,26:39,28:[1,41],30:[1,40],59:[1,42]},{28:[2,56],30:[2,56],59:[2,56]},{28:[2,57],30:[2,57],59:[2,57]},{28:[2,58],30:[2,58],59:[2,58]},{28:[2,59],30:[2,59],59:[2,59]},{19:95,22:37,23:38,26:39,28:[1,41],30:[1,40],59:[1,42]},{20:[2,48],24:[2,48],28:[2,48],30:[2,48],33:[2,48],59:[2,48]},{23:96,28:[1,51],59:[1,42]},{5:[2,21],21:[2,21],43:[2,21],51:[2,21]},{4:97,5:[2,24],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,24],27:[1,15],28:[1,16],40:[1,17],43:[2,24],46:[1,18],51:[2,24],53:[1,19],54:[1,20],55:[1,21]},{4:98,5:[2,26],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,26],27:[1,15],28:[1,16],40:[1,17],43:[2,26],46:[1,18],51:[2,26],53:[1,19],54:[1,20],55:[1,21]},{33:[1,99]},{21:[1,100]},{20:[2,37],22:77,23:78,26:79,28:[1,41],30:[1,40],32:101,33:[2,37],59:[1,42]},{20:[2,38],22:77,23:78,24:[1,65],25:103,26:79,28:[1,41],30:[1,40],32:102,33:[2,38],37:[1,66],38:[1,67],39:[1,68],59:[1,42]},{20:[2,42],24:[1,65],25:104,33:[2,42],37:[1,66],38:[1,67],39:[1,68]},{18:[1,105]},{23:106,28:[1,51],59:[1,42]},{29:[1,107]},{4:108,5:[2,53],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,53],27:[1,15],28:[1,16],40:[1,17],43:[2,53],46:[1,18],51:[2,53],53:[1,19],54:[1,20],55:[1,21]},{4:109,5:[2,54],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,54],27:[1,15],28:[1,16],40:[1,17],43:[2,54],46:[1,18],51:[2,54],53:[1,19],54:[1,20],55:[1,21]},{42:[1,110]},{23:111,28:[1,51],59:[1,42]},{42:[1,112]},{42:[1,113]},{14:114,53:[1,19]},{29:[1,115]},{23:116,28:[1,51],59:[1,42]},{4:117,5:[2,13],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,13],27:[1,15],28:[1,16],40:[1,17],43:[2,13],46:[1,18],51:[2,13],53:[1,19],54:[1,20],55:[1,21]},{20:[2,17]},{20:[2,18]},{20:[2,19]},{33:[1,118]},{5:[2,25],21:[2,25],43:[2,25],51:[2,25]},{5:[2,27],21:[2,27],43:[2,27],51:[2,27]},{21:[1,119]},{4:120,5:[2,30],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,30],27:[1,15],28:[1,16],40:[1,17],43:[2,30],46:[1,18],51:[2,30],53:[1,19],54:[1,20],55:[1,21]},{20:[2,39],33:[2,39]},{20:[2,40],33:[2,40]},{22:77,23:78,26:79,28:[1,41],30:[1,40],32:121,59:[1,42]},{22:77,23:78,26:79,28:[1,41],30:[1,40],32:122,59:[1,42]},{22:77,23:78,26:79,28:[1,41],30:[1,40],32:123,59:[1,42]},{21:[1,124]},{26:127,28:[1,128],34:[1,125],36:126},{5:[2,50],21:[2,50],43:[2,50],51:[2,50]},{5:[2,51],21:[2,51],43:[2,51],51:[2,51]},{4:129,6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],27:[1,15],28:[1,16],40:[1,17],46:[1,18],53:[1,19],54:[1,20],55:[1,21]},{20:[2,87],58:[1,130]},{47:131,49:[1,132]},{4:133,6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],27:[1,15],28:[1,16],40:[1,17],46:[1,18],53:[1,19],54:[1,20],55:[1,21]},{21:[1,134]},{23:135,28:[1,51],59:[1,42]},{21:[1,136]},{5:[2,14],21:[2,14],43:[2,14],51:[2,14]},{21:[1,137],24:[1,65],25:138,37:[1,66],38:[1,67],39:[1,68]},{4:139,5:[2,28],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,28],27:[1,15],28:[1,16],40:[1,17],43:[2,28],46:[1,18],51:[2,28],53:[1,19],54:[1,20],55:[1,21]},{5:[2,31],21:[2,31],43:[2,31],51:[2,31]},{20:[2,41],33:[2,41]},{20:[2,43],33:[2,43]},{20:[1,140]},{4:141,5:[2,55],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,55],27:[1,15],28:[1,16],40:[1,17],43:[2,55],46:[1,18],51:[2,55],53:[1,19],54:[1,20],55:[1,21]},{35:[1,142]},{5:[2,36],21:[2,36],43:[2,36],51:[2,36]},{5:[2,44],21:[2,44],43:[2,44],51:[2,44]},{31:[1,143]},{43:[1,144]},{23:55,28:[1,51],41:145,56:56,59:[1,42]},{48:146,52:[1,147]},{23:148,28:[1,51],59:[1,42]},{43:[1,149]},{5:[2,76],21:[2,76],43:[2,76],51:[2,76]},{21:[1,150]},{23:152,28:[1,51],56:151,59:[1,42]},{5:[2,46],20:[2,46],21:[2,46],24:[2,46],33:[2,46],37:[2,46],38:[2,46],39:[2,46],43:[2,46],51:[2,46]},{26:153,28:[1,154]},{5:[2,29],21:[2,29],43:[2,29],51:[2,29]},{21:[1,155]},{5:[2,52],21:[2,52],43:[2,52],51:[2,52]},{18:[1,156]},{23:157,28:[1,51],59:[1,42]},{4:159,5:[2,60],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,60],27:[1,15],28:[1,16],40:[1,17],43:[2,60],44:158,45:[1,160],46:[1,18],51:[2,60],53:[1,19],54:[1,20],55:[1,21]},{20:[2,85]},{43:[1,161]},{50:[1,162]},{50:[1,163]},{4:164,5:[2,74],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,74],27:[1,15],28:[1,16],40:[1,17],43:[2,74],46:[1,18],51:[2,74],53:[1,19],54:[1,20],55:[1,21]},{23:152,28:[1,51],56:165,59:[1,42]},{21:[1,166]},{57:[1,167]},{5:[2,47],20:[2,47],21:[2,47],24:[2,47],33:[2,47],37:[2,47],38:[2,47],39:[2,47],43:[2,47],51:[2,47]},{31:[1,71]},{4:168,5:[2,32],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,32],27:[1,15],28:[1,16],40:[1,17],43:[2,32],46:[1,18],51:[2,32],53:[1,19],54:[1,20],55:[1,21]},{22:77,23:78,26:79,28:[1,41],30:[1,40],32:169,59:[1,42]},{33:[1,170]},{5:[2,61],21:[2,61],43:[2,61],51:[2,61]},{5:[2,62],21:[2,62],43:[2,62],51:[2,62]},{40:[1,172],42:[1,171]},{4:173,5:[2,69],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,69],27:[1,15],28:[1,16],40:[1,17],43:[2,69],46:[1,18],51:[2,69],53:[1,19],54:[1,20],55:[1,21]},{4:174,6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],27:[1,15],28:[1,16],40:[1,17],46:[1,18],53:[1,19],54:[1,20],55:[1,21]},{4:175,6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],27:[1,15],28:[1,16],40:[1,17],46:[1,18],53:[1,19],54:[1,20],55:[1,21]},{5:[2,75],21:[2,75],43:[2,75],51:[2,75]},{21:[1,176]},{11:177,28:[1,178]},{23:179,28:[1,51],59:[1,42]},{5:[2,33],21:[2,33],43:[2,33],51:[2,33]},{20:[1,180]},{21:[1,181],24:[1,65],25:138,37:[1,66],38:[1,67],39:[1,68]},{4:182,6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],27:[1,15],28:[1,16],40:[1,17],46:[1,18],53:[1,19],54:[1,20],55:[1,21]},{18:[1,183]},{5:[2,70],21:[2,70],43:[2,70],51:[2,70]},{43:[2,73]},{51:[1,184]},{11:185,28:[1,178]},{20:[1,186]},{24:[1,187],29:[1,189],37:[1,188]},{21:[2,87]},{21:[1,190]},{4:191,5:[2,46],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,46],27:[1,15],28:[1,16],40:[1,17],43:[2,46],46:[1,18],51:[2,46],53:[1,19],54:[1,20],55:[1,21]},{43:[1,192]},{23:55,28:[1,51],41:193,56:56,59:[1,42]},{21:[1,194]},{20:[1,195]},{42:[1,196]},{24:[1,197]},{37:[1,198]},{28:[1,199]},{4:200,5:[2,34],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,34],27:[1,15],28:[1,16],40:[1,17],43:[2,34],46:[1,18],51:[2,34],53:[1,19],54:[1,20],55:[1,21]},{5:[2,45],21:[2,45],43:[2,45],51:[2,45]},{4:202,5:[2,63],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,63],27:[1,15],28:[1,16],40:[1,17],43:[2,63],44:201,45:[1,160],46:[1,18],51:[2,63],53:[1,19],54:[1,20],55:[1,21]},{20:[1,203]},{47:204,49:[1,132],52:[2,71]},{42:[1,205]},{4:207,6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],27:[1,15],28:[1,16],40:[1,17],43:[1,206],46:[1,18],53:[1,19],54:[1,20],55:[1,21]},{21:[1,208]},{21:[1,209]},{24:[1,65],25:210,37:[1,66],38:[1,67],39:[1,68]},{5:[2,35],21:[2,35],43:[2,35],51:[2,35]},{5:[2,64],21:[2,64],43:[2,64],51:[2,64]},{5:[2,65],21:[2,65],43:[2,65],51:[2,65]},{42:[1,211]},{52:[2,72]},{4:213,6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],27:[1,15],28:[1,16],40:[1,17],43:[1,212],46:[1,18],53:[1,19],54:[1,20],55:[1,21]},{4:214,5:[2,81],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,81],27:[1,15],28:[1,16],40:[1,17],43:[2,81],46:[1,18],51:[2,81],53:[1,19],54:[1,20],55:[1,21]},{43:[1,215]},{20:[2,53]},{20:[2,54]},{23:216,28:[1,51],59:[1,42]},{4:217,6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],27:[1,15],28:[1,16],40:[1,17],46:[1,18],53:[1,19],54:[1,20],55:[1,21]},{4:218,5:[2,77],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,77],27:[1,15],28:[1,16],40:[1,17],43:[2,77],46:[1,18],51:[2,77],53:[1,19],54:[1,20],55:[1,21]},{43:[1,219]},{5:[2,82],21:[2,82],43:[2,82],51:[2,82]},{4:220,5:[2,83],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,83],27:[1,15],28:[1,16],40:[1,17],43:[2,83],46:[1,18],51:[2,83],53:[1,19],54:[1,20],55:[1,21]},{21:[1,221]},{43:[1,222]},{5:[2,78],21:[2,78],43:[2,78],51:[2,78]},{4:223,5:[2,79],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,79],27:[1,15],28:[1,16],40:[1,17],43:[2,79],46:[1,18],51:[2,79],53:[1,19],54:[1,20],55:[1,21]},{5:[2,84],21:[2,84],43:[2,84],51:[2,84]},{20:[2,55]},{4:225,5:[2,66],6:3,7:4,8:5,9:6,10:7,11:8,12:9,13:10,14:11,15:12,16:13,17:[1,14],21:[2,66],27:[1,15],28:[1,16],40:[1,17],43:[2,66],44:224,45:[1,160],46:[1,18],51:[2,66],53:[1,19],54:[1,20],55:[1,21]},{5:[2,80],21:[2,80],43:[2,80],51:[2,80]},{5:[2,67],21:[2,67],43:[2,67],51:[2,67]},{5:[2,68],21:[2,68],43:[2,68],51:[2,68]}],
defaultActions: {22:[2,1],56:[2,86],93:[2,17],94:[2,18],95:[2,19],145:[2,85],174:[2,73],179:[2,87],204:[2,72],208:[2,53],209:[2,54],221:[2,55]},
parseError: function parseError(str,hash){if(hash.recoverable){this.trace(str)}else{throw new Error(str)}},
parse: function parse(input) {
    var self = this, stack = [0], vstack = [null], lstack = [], table = this.table, yytext = '', yylineno = 0, yyleng = 0, recovering = 0, TERROR = 2, EOF = 1;
    var args = lstack.slice.call(arguments, 1);
    this.lexer.setInput(input);
    this.lexer.yy = this.yy;
    this.yy.lexer = this.lexer;
    this.yy.parser = this;
    if (typeof this.lexer.yylloc == 'undefined') {
        this.lexer.yylloc = {};
    }
    var yyloc = this.lexer.yylloc;
    lstack.push(yyloc);
    var ranges = this.lexer.options && this.lexer.options.ranges;
    if (typeof this.yy.parseError === 'function') {
        this.parseError = this.yy.parseError;
    } else {
        this.parseError = Object.getPrototypeOf(this).parseError;
    }
    function popStack(n) {
        stack.length = stack.length - 2 * n;
        vstack.length = vstack.length - n;
        lstack.length = lstack.length - n;
    }
    function lex() {
        var token;
        token = self.lexer.lex() || EOF;
        if (typeof token !== 'number') {
            token = self.symbols_[token] || token;
        }
        return token;
    }
    var symbol, preErrorSymbol, state, action, a, r, yyval = {}, p, len, newState, expected;
    while (true) {
        state = stack[stack.length - 1];
        if (this.defaultActions[state]) {
            action = this.defaultActions[state];
        } else {
            if (symbol === null || typeof symbol == 'undefined') {
                symbol = lex();
            }
            action = table[state] && table[state][symbol];
        }
                    if (typeof action === 'undefined' || !action.length || !action[0]) {
                var errStr = '';
                expected = [];
                for (p in table[state]) {
                    if (this.terminals_[p] && p > TERROR) {
                        expected.push('\'' + this.terminals_[p] + '\'');
                    }
                }
                if (this.lexer.showPosition) {
                    errStr = 'Error de sintaxis en la línea ' + (yylineno + 1) + ':\n' + this.lexer.showPosition() + '\nSe esperaba ' + expected.join(', ') + ', en vez de \'' + (this.terminals_[symbol] || symbol) + '\''; 
                    $("#Error").css("color","red");
                    $("#result").html('Error de sintaxis en la linea ' + (yylineno + 1) + ':<br>' + this.lexer.showPosition2()+'<br>Se esperaba ' + expected.join(', &emsp;') + ',&emsp; en vez de  \'' + (this.terminals_[symbol] || symbol) + '\'');
                } else {
                    errStr = 'Error de sintaxis en la línea ' + (yylineno + 1) + ': Inesperado ' + (symbol == EOF ? 'al final de la linea' : '\'' + (this.terminals_[symbol] || symbol) + '\''); 
                    $("#Error").css("color","red");
                    $("#result").html('Error de sintaxis en la linea ' + (yylineno + 1) + ': Inesperado ' + (symbol == EOF ? 'al final de la linea' : '\'' + (this.terminals_[symbol] || symbol) + '\''));
                }
                var parrafo=errStr.split("\n");
                var parrafos=parrafo[0]+" "+parrafo[parrafo.length-1];
                responsiveVoice.speak(parrafos, "Spanish Female"); 

                this.parseError(errStr, {
                    text: this.lexer.match,
                    token: this.terminals_[symbol] || symbol,
                    line: this.lexer.yylineno,
                    loc: yyloc,
                    expected: expected
                });
            }
        if (action[0] instanceof Array && action.length > 1) {
            throw new Error('Parse Error: multiple actions possible at state: ' + state + ', token: ' + symbol);
        }
        switch (action[0]) {
        case 1:
            stack.push(symbol);
            vstack.push(this.lexer.yytext);
            lstack.push(this.lexer.yylloc);
            stack.push(action[1]);
            symbol = null;
            if (!preErrorSymbol) {
                yyleng = this.lexer.yyleng;
                yytext = this.lexer.yytext;
                yylineno = this.lexer.yylineno;
                yyloc = this.lexer.yylloc;
                if (recovering > 0) {
                    recovering--;
                }
            } else {
                symbol = preErrorSymbol;
                preErrorSymbol = null;
            }
            break;
        case 2:
            len = this.productions_[action[1]][1];
            yyval.$ = vstack[vstack.length - len];
            yyval._$ = {
                first_line: lstack[lstack.length - (len || 1)].first_line,
                last_line: lstack[lstack.length - 1].last_line,
                first_column: lstack[lstack.length - (len || 1)].first_column,
                last_column: lstack[lstack.length - 1].last_column
            };
            if (ranges) {
                yyval._$.range = [
                    lstack[lstack.length - (len || 1)].range[0],
                    lstack[lstack.length - 1].range[1]
                ];
            }
            r = this.performAction.apply(yyval, [
                yytext,
                yyleng,
                yylineno,
                this.yy,
                action[1],
                vstack,
                lstack
            ].concat(args));
            if (typeof r !== 'undefined') {
                return r;
            }
            if (len) {
                stack = stack.slice(0, -1 * len * 2);
                vstack = vstack.slice(0, -1 * len);
                lstack = lstack.slice(0, -1 * len);
            }
            stack.push(this.productions_[action[1]][0]);
            vstack.push(yyval.$);
            lstack.push(yyval._$);
            newState = table[stack[stack.length - 2]][stack[stack.length - 1]];
            stack.push(newState);
            break;
        case 3:
            return true;
        }
    }
    return true;
}};
/* generated by jison-lex 0.2.1 */
var lexer = (function(){
var lexer = {

EOF:1,

parseError:function parseError(str,hash){if(this.yy.parser){this.yy.parser.parseError(str,hash)}else{throw new Error(str)}},

// resets the lexer, sets new input
setInput:function (input){this._input=input;this._more=this._backtrack=this.done=false;this.yylineno=this.yyleng=0;this.yytext=this.matched=this.match="";this.conditionStack=["INITIAL"];this.yylloc={first_line:1,first_column:0,last_line:1,last_column:0};if(this.options.ranges){this.yylloc.range=[0,0]}this.offset=0;return this},

// consumes and returns one char from the input
input:function (){var ch=this._input[0];this.yytext+=ch;this.yyleng++;this.offset++;this.match+=ch;this.matched+=ch;var lines=ch.match(/(?:\r\n?|\n).*/g);if(lines){this.yylineno++;this.yylloc.last_line++}else{this.yylloc.last_column++}if(this.options.ranges){this.yylloc.range[1]++}this._input=this._input.slice(1);return ch},

// unshifts one char (or a string) into the input
unput:function (ch){var len=ch.length;var lines=ch.split(/(?:\r\n?|\n)/g);this._input=ch+this._input;this.yytext=this.yytext.substr(0,this.yytext.length-len-1);this.offset-=len;var oldLines=this.match.split(/(?:\r\n?|\n)/g);this.match=this.match.substr(0,this.match.length-1);this.matched=this.matched.substr(0,this.matched.length-1);if(lines.length-1){this.yylineno-=lines.length-1}var r=this.yylloc.range;this.yylloc={first_line:this.yylloc.first_line,last_line:this.yylineno+1,first_column:this.yylloc.first_column,last_column:lines?(lines.length===oldLines.length?this.yylloc.first_column:0)+oldLines[oldLines.length-lines.length].length-lines[0].length:this.yylloc.first_column-len};if(this.options.ranges){this.yylloc.range=[r[0],r[0]+this.yyleng-len]}this.yyleng=this.yytext.length;return this},

// When called from action, caches matched text and appends it on next action
more:function (){this._more=true;return this},

// When called from action, signals the lexer that this rule fails to match the input, so the next matching rule (regex) should be tested instead.
reject:function (){if(this.options.backtrack_lexer){this._backtrack=true}else{return this.parseError("Lexical error on line "+(this.yylineno+1)+". You can only invoke reject() in the lexer when the lexer is of the backtracking persuasion (options.backtrack_lexer = true).\n"+this.showPosition(),{text:"",token:null,line:this.yylineno})}return this},

// retain first n characters of the match
less:function (n){this.unput(this.match.slice(n))},

// displays already matched input, i.e. for error messages
pastInput:function (){var past=this.matched.substr(0,this.matched.length-this.match.length);return(past.length>20?"...":"")+past.substr(-20).replace(/\n/g,"")},

// displays upcoming input, i.e. for error messages
upcomingInput:function (){
                var next=this.match;
                if(next.length<20){
                    next+=this._input.substr(0,20-next.length)
                }
                return(next.substr(0,20)+(next.length>20?"...":"")).replace(/\n/g,"")
            },

// displays the character position where the lexing error occurred, i.e. for error messages
showPosition:function (){
                var pre=this.pastInput();
                var c=new Array(pre.length+1).join("-");
                return pre+this.upcomingInput()+"\n"+c+"^"
            },

showPosition2:function (){
            var pre=this.pastInput();
            var c=new Array(pre.length+1).join("-");
            return pre+this.upcomingInput()+"<br>"+c+"--^"},

// test the lexed token: return FALSE when not a match, otherwise return token
test_match:function (match,indexed_rule){var token,lines,backup;if(this.options.backtrack_lexer){backup={yylineno:this.yylineno,yylloc:{first_line:this.yylloc.first_line,last_line:this.last_line,first_column:this.yylloc.first_column,last_column:this.yylloc.last_column},yytext:this.yytext,match:this.match,matches:this.matches,matched:this.matched,yyleng:this.yyleng,offset:this.offset,_more:this._more,_input:this._input,yy:this.yy,conditionStack:this.conditionStack.slice(0),done:this.done};if(this.options.ranges){backup.yylloc.range=this.yylloc.range.slice(0)}}lines=match[0].match(/(?:\r\n?|\n).*/g);if(lines){this.yylineno+=lines.length}this.yylloc={first_line:this.yylloc.last_line,last_line:this.yylineno+1,first_column:this.yylloc.last_column,last_column:lines?lines[lines.length-1].length-lines[lines.length-1].match(/\r?\n?/)[0].length:this.yylloc.last_column+match[0].length};this.yytext+=match[0];this.match+=match[0];this.matches=match;this.yyleng=this.yytext.length;if(this.options.ranges){this.yylloc.range=[this.offset,this.offset+=this.yyleng]}this._more=false;this._backtrack=false;this._input=this._input.slice(match[0].length);this.matched+=match[0];token=this.performAction.call(this,this.yy,this,indexed_rule,this.conditionStack[this.conditionStack.length-1]);if(this.done&&this._input){this.done=false}if(token){return token}else if(this._backtrack){for(var k in backup){this[k]=backup[k]}return false}return false},

// return next match in input
next:function (){if(this.done){return this.EOF}if(!this._input){this.done=true}var token,match,tempMatch,index;if(!this._more){this.yytext="";this.match=""}var rules=this._currentRules();for(var i=0;i<rules.length;i++){tempMatch=this._input.match(this.rules[rules[i]]);if(tempMatch&&(!match||tempMatch[0].length>match[0].length)){match=tempMatch;index=i;if(this.options.backtrack_lexer){token=this.test_match(tempMatch,rules[i]);if(token!==false){return token}else if(this._backtrack){match=false;continue}else{return false}}else if(!this.options.flex){break}}}if(match){token=this.test_match(match,rules[index]);if(token!==false){return token}return false}if(this._input===""){return this.EOF}else{return this.parseError("Lexical error on line "+(this.yylineno+1)+". Unrecognized text.\n"+this.showPosition(),{text:"",token:null,line:this.yylineno})}},

// return next match that has a token
lex:function lex(){var r=this.next();if(r){return r}else{return this.lex()}},

// activates a new lexer condition state (pushes the new lexer condition state onto the condition stack)
begin:function begin(condition){this.conditionStack.push(condition)},

// pop the previously active lexer condition state off the condition stack
popState:function popState(){var n=this.conditionStack.length-1;if(n>0){return this.conditionStack.pop()}else{return this.conditionStack[0]}},

// produce the lexer rule set which is active for the currently active lexer condition state
_currentRules:function _currentRules(){if(this.conditionStack.length&&this.conditionStack[this.conditionStack.length-1]){return this.conditions[this.conditionStack[this.conditionStack.length-1]].rules}else{return this.conditions["INITIAL"].rules}},

// return the currently active lexer condition state; when an index argument is provided it produces the N-th previous condition state, if available
topState:function topState(n){n=this.conditionStack.length-1-Math.abs(n||0);if(n>=0){return this.conditionStack[n]}else{return"INITIAL"}},

// alias for begin(condition)
pushState:function pushState(condition){this.begin(condition)},

// return the number of states currently on the stack
stateStackSize:function stateStackSize(){return this.conditionStack.length},
options: {},
performAction: function anonymous(yy,yy_,$avoiding_name_collisions,YY_START
/*``*/) {

var YYSTATE=YY_START;
switch($avoiding_name_collisions) {
case 0:/* saltar los comentarios*/  
break;
case 1:/* skip whitespace */
break;
case 2:return 30
break;
case 3:return 30
break;
case 4:return 27
break;
case 5:return 40
break;
case 6:return 45
break;
case 7:return 46
break;
case 8:return 49
break;
case 9:return 51
break;
case 10:return 52
break;
case 11:return 17
break;
case 12:return 53
break;
case 13:return 55
break;
case 14:return 54
break;
case 15:return 34
break;
case 16:return 35
break;
case 17:return 58
break;
case 18:return 58
break;
case 19:return 58
break;
case 20:return 58
break;
case 21:return 59
break;
case 22:return 29
break;
case 23:return 24
break;
case 24:return 37
break;
case 25:return 38
break;
case 26:return 39
break;
case 27:return 18
break;
case 28:return 20
break;
case 29:return 42
break;
case 30:return 43
break;
case 31:return 31
break;
case 32:return 33
break;
case 33:return 50
break;
case 34:return 21
break;
case 35:return 57
break;
case 36:return 57
break;
case 37:return 57
break;
case 38:return 57
break;
case 39:return 57
break;
case 40:return 57
break;
case 41:return 57
break;
case 42:return 28
break;
case 43:return 5
break;
case 44:return 'INVALID'
break;
}
},
rules: [/^(?:<.*>)/,/^(?:\s+)/,/^(?:".*")/,/^(?:'.*')/,/^(?:decla\b)/,/^(?:si\b)/,/^(?:sino\b)/,/^(?:bandera\b)/,/^(?:caso\b)/,/^(?:cortar\b)/,/^(?:pordefecto\b)/,/^(?:pantallazo\b)/,/^(?:c_w\b)/,/^(?:c_f\b)/,/^(?:hacer\b)/,/^(?:nuevo\b)/,/^(?:arreglo\b)/,/^(?:\|\|)/,/^(?:or\b)/,/^(?:and\b)/,/^(?:&&)/,/^(?:-*[0-9]+(\.[0-9]+)?\b)/,/^(?:<-)/,/^(?:\+)/,/^(?:-)/,/^(?:\*)/,/^(?:\/)/,/^(?:\()/,/^(?:\))/,/^(?:\[)/,/^(?:\])/,/^(?:\{)/,/^(?:\})/,/^(?::)/,/^(?:,)/,/^(?:==)/,/^(?:>)/,/^(?:<)/,/^(?:>=)/,/^(?:<=)/,/^(?:!=)/,/^(?:<>)/,/^(?:[A-Za-z]+[0-9]*)/,/^(?:$)/,/^(?:.)/],
conditions: {"INITIAL":{"rules":[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44],"inclusive":true}}
};
return lexer;
})();
parser.lexer = lexer;
function Parser () {
  this.yy = {};
}
Parser.prototype = parser;parser.Parser = Parser;
return new Parser;
})();


if (typeof require !== 'undefined' && typeof exports !== 'undefined') {
exports.parser = parser;
exports.Parser = parser.Parser;
exports.parse = function () { return parser.parse.apply(parser, arguments); };
exports.main = function commonjsMain(args){if(!args[1]){console.log("Usage: "+args[0]+" FILE");process.exit(1)}var source=require("fs").readFileSync(require("path").normalize(args[1]),"utf8");return exports.parser.parse(source)};
if (typeof module !== 'undefined' && require.main === module) {
  exports.main(process.argv.slice(1));
}
}